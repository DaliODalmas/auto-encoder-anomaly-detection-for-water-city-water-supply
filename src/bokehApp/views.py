from django.http import HttpResponse
from django.shortcuts import render,render_to_response
import tensorflow as tf
from bokeh.plotting import figure
from bokeh.embed import components
from bokeh.models import HoverTool, LassoSelectTool, WheelZoomTool, PointDrawTool, ColumnDataSource
import time
from bokeh.palettes import Category20c, Spectral6
from bokeh.transform import cumsum
from .models import Products
from numpy import pi
import pandas as pd
from bokeh.resources import CDN
from .models import Products
from django.db import connection
import numpy as np
from keras.models import load_model
from sklearn.externals import joblib
from sklearn.preprocessing import MinMaxScaler
# load model
model = load_model('./auto_encoder/cloud_model.h5')
scaler = joblib.load("./auto_encoder/scaler_data")
def dbwrite(request):
    data=pd.read_csv("./data/test.csv")
    
    
    for i in data.values:
        
        time.sleep(3.0)
        # reading database
        obj=str(Products.objects.all().query)
        df = pd.read_sql_query(obj,connection)

        #getting the predictions
        upper=0.05385545972182722
        sdf=scaler.transform(df[["dauglas","nothern"]].tail(4).values)
        pdf=sdf.reshape((1,4,2))

        anomaly=np.mean(np.abs(pdf-model.predict(pdf)))>upper
        print(anomaly)

        #writing to database
        data_instance=Products.objects.create(date=i[0],dauglas=i[1],nothern=i[2],anomaly=anomaly)

    return HttpResponse("<h1>Test instances have finished running</h1>")



def home(request):
    # reading database
    obj=str(Products.objects.all().query)
    df = pd.read_sql_query(obj,connection)
    
    an=pd.DataFrame(df[["nothern","anomaly"]].groupby(["anomaly"]).count()).values

    x ={
        'normal':an[0][0],
        'not normal':an[1][0]
    }
    data = pd.Series(x).reset_index(name='value').rename(columns={'index':'anomaly'})
    data['angle'] = data['value']/data['value'].sum() * 2*pi
    print(data)
    data['color'] = ["green","red"]
    p = figure(plot_height=350, title="Fraction of anomalies detected", toolbar_location=None,
            tools="hover", tooltips="@anomaly: @value", x_range=(-0.5, 1.0))

    p.wedge(x=0, y=1, radius=0.4,
            start_angle=cumsum('angle', include_zero=True), end_angle=cumsum('angle'),
            line_color="white", fill_color='color', legend='anomaly', source=data)
    p.background_fill_color = "lightgrey"
    p.border_fill_color = "whitesmoke"
    p.min_border_left = 40
    p.min_border_right = 40
    p.outline_line_width = 7
    p.outline_line_alpha = 0.2
    p.outline_line_color = "purple"
    script1, div1 = components(p)

    result='not normal' if df.tail(1)["anomaly"].values[0] else 'normal'


    return render(request, 'home.html' , {'script1': script1, 'div1':div1,'result':result, 'anomalies':x["normal"],'nomalies':x["not normal"]})
